Create view vw_gl_transactions

as


SELECT


       tr.ReferenceNumber

      ,tr.SequenceNumber

      ,tr.Type as TransactionType

      ,tr.GLAccount


  ,a.FormattedAccountNumber

     --
     -- fund
     -- 4 or 6 expense / revenue
     -- 5 digit department
     -- 6 digit
     --

  ,substring(a.AccountNumber, 7,5) as ObjectNo


  ,substring(a.AccountNumber, 12,2) as ProgramNo


  ,substring(a.FormattedAccountNumber, 9,8) as ObjectProgramNo


  ,a.Title


  ,a.ExpandedTitle


  ,a.NormalBalance


  ,ac.AccountCode


  ,ac.AccountType


  --,left(fund.FormattedAccountNumber,3) as FundNumber

      ,left( a.FormattedAccountNumber,3) as FundNumber

      ,fund.Title as Fund


  ,left(dept.FormattedAccountNumber,7) as DepartmentNumber


  ,dept.Title as Department




      ,tr.Date as TransactionDate


      ,tr.Description as TransactionDescription


      ,tr.Amount


      ,tr.DepositAmount


      ,tr.CheckAmount


      ,tr.Allocations


      ,tr.JobNumber


      --,tr.PMUpdated


      --,tr.PMUpdateRequired


      --,tr.CreatedBy


      ,tr.CreatedDate


      --,tr.LastModifiedBy


      ,tr.LastModifiedDate


      ,tr.tblActivityID


      ,tr.tblCheckBankID


      ,tr.tblCheckPeriodDateID


      ,tr.tblDepositBankID


      ,tr.tblDepositPeriodDateID


      ,tr.tblPeriodDateID


      ,tr.ID as TransactionID


      ,tr.CommentDTS


      ,tr.AllocationsDTS


      ,tr.tblJournalCodeID


      ,tr.Comment


      --,tr.NHVersion

      --,tr.DrillThroughGUID


  ,tjc.JournalCode


  ,tjc.Title as JournalCodeName


  ,tpd.GLPeriod


  ,tpd.PeriodDate


  ,tpd.PeriodNumber


  ,apck.CheckNumber


  ,apck.CheckIssueDate


  ,apck.PayeeName


  ,apivd.Description as InvoiceDetailDescription

  ,apiv.InvoiceNumber


  ,apiv.InvoiceDate


  ,apv.Name as VendorName



  FROM GL0_MasonCityIA.dbo.tblTransaction  tr

  join GL0_MasonCityIA.dbo.tblAccount a on a.AccountNumber = tr.GLAccount

  left join GL0_MasonCityIA.dbo.tblPeriodDate tpd on tpd.ID = tr.tblPeriodDateID


  left join GL0_MasonCityIA.dbo.tblAccountCode ac on ac.ID = a.tblAccountCodeID


  left join GL0_MasonCityIA.dbo.tblAccountHeader dept on dept.MaskCharacter = 'D' and left(a.AccountNumber,6) = left(dept.AccountNumber,6)


  left join GL0_MasonCityIA.dbo.tblAccountHeader fund on fund.MaskCharacter = 'F' and a.AccountNumber = fund.AccountNumber


  left join GL0_MasonCityIA.dbo.tblJournalCode tjc on tjc.ID = tr.tblJournalCodeID


  left join AP0_MasonCityIA.dbo.tblCheckDetail apcd on apcd.GLGUID = tr.DrillThroughGUID and tr.DrillThroughGUID != '00000000-0000-0000-0000-000000000000'


  left join AP0_MasonCityIA.dbo.tblCheck apck on apck.ID = apcd.tblCheckID


  left join AP0_MasonCityIA.dbo.tblInvoiceDetail apivd on apivd.ID = apcd.tblInvoiceDetailID


  left join AP0_MasonCityIA.dbo.tblInvoice apiv on apiv.ID = apivd.tblInvoiceID


  left join AP0_MasonCityIA.dbo.tblVendor apv on apv.ID = apiv.tblVendorID




  --where tpd.PeriodDate > '01/01/2017'
